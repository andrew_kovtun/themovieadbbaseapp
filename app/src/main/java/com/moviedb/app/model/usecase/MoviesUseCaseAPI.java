package com.moviedb.app.model.usecase;

import com.moviedb.app.model.entity.Movie;
import com.moviedb.app.model.entity.MovieDetails;

import java.util.List;

import androidx.annotation.NonNull;
import io.reactivex.Observable;

public interface MoviesUseCaseAPI {

    @NonNull
    Observable<List<Movie>> getMovies(int page);

    @NonNull
    Observable<MovieDetails> getMovieDetails(long id);

    @NonNull
    Observable<List<Movie>> searchMovies(@NonNull String query);
}
