package com.moviedb.app.ui.entity;

import com.moviedb.app.model.entity.Movie;
import com.moviedb.app.ui.state.State;

import java.util.List;

public class MovieUIResponse extends UIResponse<List<Movie>> {

    public MovieUIResponse(State state, List<Movie> commonObject) {
        super(state, commonObject);
    }
}
