package com.moviedb.app.model.entity;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class MovieDetails {
    @SerializedName("budget")
    private final int budget;
    @SerializedName("revenue")
    private final int revenue;
    @SerializedName("status")
    private final String status;
    @SerializedName("tagline")
    private final String tagline;
    @SerializedName("title")
    private final String title;
    @SerializedName("vote_average")
    private final float voteAverage;
    @SerializedName("runtime")
    private final float runtime;
    @SerializedName("overview")
    private final String overview;
    @SerializedName("poster_path")
    private final String posterUri;

    public MovieDetails(int budget, int revenue, String status, String tagline, String title, float voteAverage, float runtime, String overview, String posterUri) {
        this.budget = budget;
        this.revenue = revenue;
        this.status = status;
        this.tagline = tagline;
        this.title = title;
        this.voteAverage = voteAverage;
        this.runtime = runtime;
        this.overview = overview;
        this.posterUri = posterUri;
    }

    public String getStatus() {
        return status;
    }

    public String getTagline() {
        return tagline;
    }

    public String getTitle() {
        return title;
    }

    public float getVoteAverage() {
        return voteAverage;
    }

    public float getRuntime() {
        return runtime;
    }

    public String getPosterUri() {
        return posterUri;
    }

    public int getBudget() {
        return budget;
    }

    public int getRevenue() {
        return revenue;
    }

    public String getOverview() {
        return overview;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MovieDetails)) return false;
        MovieDetails that = (MovieDetails) o;
        return Float.compare(that.getVoteAverage(), getVoteAverage()) == 0 &&
                Float.compare(that.getRuntime(), getRuntime()) == 0 &&
                Objects.equals(getStatus(), that.getStatus()) &&
                Objects.equals(getTagline(), that.getTagline()) &&
                Objects.equals(getTitle(), that.getTitle());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getStatus(), getTagline(), getTitle(), getVoteAverage(), getRuntime());
    }
}
