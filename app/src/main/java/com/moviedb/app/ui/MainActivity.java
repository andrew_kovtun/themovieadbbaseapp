package com.moviedb.app.ui;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.android.material.snackbar.Snackbar;
import com.jakewharton.rxbinding3.widget.RxSearchView;
import com.moviedb.app.R;
import com.moviedb.app.ui.adapter.MoviesAdapter;
import com.moviedb.app.ui.entity.MovieUIResponse;
import com.moviedb.app.utils.RouterUtils;
import com.moviedb.app.viewmodel.MoviesListScreenViewModel;
import com.moviedb.app.viewmodel.SearchViewModel;

import java.util.concurrent.TimeUnit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;

public class MainActivity extends AppCompatActivity implements MoviesAdapter.ItemClickListener {
    private static final String TAG = "{MainActivity}";
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private SearchViewModel<MovieUIResponse> viewModel;
    private MoviesAdapter adapter;
    private ProgressBar progressView;
    private ViewGroup baseContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, String.format("onCreate(); savedInstanceState=[%s]", savedInstanceState));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Bundle bundle = getIntent().getExtras();
        initUI();
        viewModel = ViewModelProviders.of(this).get(MoviesListScreenViewModel.class);
        viewModel.onCreate(bundle);
        viewModel.getStateLiveData().observe(this, moviesDataObserver);
    }

    private void initUI() {
        RecyclerView recyclerView = findViewById(R.id.movies_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new MoviesAdapter();
        adapter.setOnItemClickListener(this);
        recyclerView.setAdapter(adapter);
        progressView = findViewById(R.id.progress_view);
        baseContainer = findViewById(R.id.container);
        compositeDisposable.add(
                RxSearchView.queryTextChanges(findViewById(R.id.search_view))
                        .debounce(500, TimeUnit.MILLISECONDS)
                        .distinctUntilChanged()
                        .map(String::valueOf)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(query -> viewModel.onSearch(query)));
    }

    private final Observer<MovieUIResponse> moviesDataObserver = movieUIResponse -> {
        Log.d(TAG, String.format("onDataChange(); data=[%s]", movieUIResponse));
        switch (movieUIResponse.getState()) {
            case COMPLETE:
                adapter.clear();
                adapter.addAll(movieUIResponse.getCommonObject());
                break;
            case STOP_LOADING:
                stopLoading();
                break;
            case START_LOADING:
                startLoading();
                break;
            case ERROR:
                showError();
        }
    };

    private void startLoading() {
        Log.d(TAG, "startLoading()");
        progressView.setVisibility(View.VISIBLE);
    }

    private void stopLoading() {
        Log.d(TAG, "stopLoading()");
        progressView.setVisibility(View.GONE);
    }

    private void showError() {
        Log.d(TAG, "showError()");
        Snackbar.make(baseContainer, getString(R.string.error_common), Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void onItemClick(View view, int position) {
        Log.d(TAG, String.format("onItemClick(); position=[%s]", position));
        startActivity(RouterUtils.buildMovieDetailsRouteAction(this, adapter.getItem(position).getId()));
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy()");
        super.onDestroy();
        compositeDisposable.dispose();
        viewModel.getStateLiveData().removeObservers(this);
    }
}
