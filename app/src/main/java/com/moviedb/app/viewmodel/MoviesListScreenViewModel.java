package com.moviedb.app.viewmodel;

import android.os.Bundle;
import android.text.TextUtils;

import com.moviedb.app.model.entity.Movie;
import com.moviedb.app.model.repository.Cache;
import com.moviedb.app.model.repository.SimpleCache;
import com.moviedb.app.model.usecase.MoviesUseCaseAPI;
import com.moviedb.app.model.usecase.NetworkService;
import com.moviedb.app.ui.entity.MovieUIResponse;
import com.moviedb.app.ui.state.State;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class MoviesListScreenViewModel extends ViewModel implements SearchViewModel<MovieUIResponse> {
    private final MoviesUseCaseAPI movieUseCases = NetworkService.getInstance();
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private final MutableLiveData<MovieUIResponse> stateLiveData = new MutableLiveData<>();
    private final Cache<Movie> movieCache = new SimpleCache<>();

    @Override
    public void onCreate(@Nullable Bundle bundle) {
        loadMovies();
    }

    @Override
    public LiveData<MovieUIResponse> getStateLiveData() {
        return stateLiveData;
    }

    private void loadMovies() {
        if (movieCache.hasData()) {
            stateLiveData.setValue(new MovieUIResponse(State.COMPLETE, movieCache.getAll()));
            return;
        }
        compositeDisposable.add(Observable.range(1, 5)
                .flatMap(movieUseCases::getMovies)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(s -> {
                    movieCache.clear();
                    stateLiveData.setValue(new MovieUIResponse(State.START_LOADING, null));
                })
                .doOnTerminate(() -> {
                    stateLiveData.setValue(new MovieUIResponse(State.STOP_LOADING, null));
                    stateLiveData.setValue(new MovieUIResponse(State.COMPLETE, movieCache.getAll()));
                })
                .subscribe(
                        movieCache::put,
                        e -> stateLiveData.setValue(new MovieUIResponse(State.ERROR, null))));
    }

    @Override
    public void onSearch(@NonNull String query) {
        if (TextUtils.isEmpty(query)) {
            loadMovies();
            return;
        }
        compositeDisposable.add(movieUseCases.searchMovies(query)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(s -> {
                    stateLiveData.setValue(new MovieUIResponse(State.START_LOADING, null));
                })
                .doOnTerminate(() -> stateLiveData.setValue(new MovieUIResponse(State.STOP_LOADING, null)))
                .subscribe(
                        movies -> stateLiveData.setValue(new MovieUIResponse(State.COMPLETE, movies)),
                        e -> stateLiveData.setValue(new MovieUIResponse(State.ERROR, null))));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        movieCache.clear();
        compositeDisposable.dispose();
    }
}
