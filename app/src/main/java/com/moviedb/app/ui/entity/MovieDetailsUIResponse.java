package com.moviedb.app.ui.entity;

import com.moviedb.app.model.entity.MovieDetails;
import com.moviedb.app.ui.state.State;

public class MovieDetailsUIResponse extends UIResponse<MovieDetails> {

    public MovieDetailsUIResponse(State state, MovieDetails commonObject) {
        super(state, commonObject);
    }
}
