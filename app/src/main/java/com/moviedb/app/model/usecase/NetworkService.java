package com.moviedb.app.model.usecase;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.moviedb.app.model.entity.Movie;
import com.moviedb.app.model.entity.MovieDetails;
import com.moviedb.app.model.entity.MoviesBase;
import com.moviedb.app.utils.NetworkUtils;

import java.util.List;

import androidx.annotation.NonNull;
import io.reactivex.Observable;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkService implements MoviesUseCaseAPI {
    private static NetworkService instance;

    private final MoviesRxAPI moviesRxAPI;

    public static NetworkService getInstance() {
        if (instance == null) {
            instance = new NetworkService();
        }
        return instance;
    }

    private NetworkService() {
        Retrofit retrofit = buildRetrofitService();
        moviesRxAPI = retrofit.create(MoviesRxAPI.class);
    }

    private Retrofit buildRetrofitService() {
        return new Retrofit.Builder()
                .baseUrl(NetworkUtils.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @NonNull
    @Override
    public Observable<List<Movie>> getMovies(int page) {
        return moviesRxAPI.listPopularMovies(NetworkUtils.API_KEY, NetworkUtils.DEFAULT_LOCALE, page).map(MoviesBase::getMovieCollection);
    }

    @NonNull
    @Override
    public Observable<MovieDetails> getMovieDetails(long id) {
        return moviesRxAPI.getMovieDetails(id, NetworkUtils.API_KEY, NetworkUtils.DEFAULT_LOCALE);
    }

    @NonNull
    @Override
    public Observable<List<Movie>> searchMovies(@NonNull String query) {
        return moviesRxAPI.searchMovies(NetworkUtils.API_KEY, NetworkUtils.DEFAULT_LOCALE, query, NetworkUtils.DEFAULT_PAGE, NetworkUtils.DEFAULT_ADULT).map(MoviesBase::getMovieCollection);
    }
}
