package com.moviedb.app.model.repository;

import java.util.List;

public interface Cache<T> {

    void put(List<T> data);

    List<T> getAll();

    void clear();

    boolean hasData();
}
