package com.moviedb.app.utils;

public final class NetworkUtils {
    public static final String BASE_URL = "https://api.themoviedb.org/3/";
    public static final String BASE_IMG_URL = "https://image.tmdb.org/t/p/w500";
    public static final String API_KEY = "433e79120b306f8666658d926876eba1";
    public static final String DEFAULT_LOCALE = "en-us";
    public static final int DEFAULT_PAGE = 1;
    public static final boolean DEFAULT_ADULT = false;
}
