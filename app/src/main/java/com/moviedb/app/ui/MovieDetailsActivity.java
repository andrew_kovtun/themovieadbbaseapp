package com.moviedb.app.ui;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.moviedb.app.R;
import com.moviedb.app.model.entity.MovieDetails;
import com.moviedb.app.ui.entity.MovieDetailsUIResponse;
import com.moviedb.app.utils.NetworkUtils;
import com.moviedb.app.viewmodel.BaseScreenViewModel;
import com.moviedb.app.viewmodel.DetailsScreenViewModel;
import com.squareup.picasso.Picasso;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

public class MovieDetailsActivity extends AppCompatActivity {
    private static final String TAG = "{MovieDetailsActivity}";
    private BaseScreenViewModel<MovieDetailsUIResponse> viewModel;
    private ViewGroup baseContainer;
    private ImageView posterImg;
    private TextView titleTxt;
    private TextView budgetTxt;
    private TextView revenueTxt;
    private TextView overviewTxt;
    private ProgressBar posterProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Bundle bundle = getIntent().getExtras();
        initUI();
        viewModel = ViewModelProviders.of(this).get(DetailsScreenViewModel.class);
        viewModel.onCreate(bundle);
        viewModel.getStateLiveData().observe(this, movieDetailsObserver);
    }

    private void initUI() {
        posterImg = findViewById(R.id.poster_big_img);
        titleTxt = findViewById(R.id.title_txt);
        budgetTxt = findViewById(R.id.budget_txt);
        revenueTxt = findViewById(R.id.revenue_txt);
        overviewTxt = findViewById(R.id.overview_txt);
        posterProgress = findViewById(R.id.poster_progress);
        baseContainer = findViewById(R.id.base_container);
    }

    private final Observer<MovieDetailsUIResponse> movieDetailsObserver = response -> {
        switch (response.getState()) {
            case COMPLETE:
                onComplete(response.getCommonObject());
                break;
            case STOP_LOADING:
                stopLoading();
                break;
            case START_LOADING:
                startLoading();
                break;
            case ERROR:
                showError();
        }
    };

    private void onComplete(MovieDetails movieDetails) {
        titleTxt.setText(movieDetails.getTitle());
        budgetTxt.setText(String.valueOf(movieDetails.getBudget()));
        revenueTxt.setText(String.valueOf(movieDetails.getRevenue()));
        overviewTxt.setText(movieDetails.getOverview());
        Picasso.get()
                .load(NetworkUtils.BASE_IMG_URL + movieDetails.getPosterUri())
                .fit()
                .centerCrop()
                .into(posterImg);
    }

    private void startLoading() {
        Log.d(TAG, "startLoading()");
        posterProgress.setVisibility(View.VISIBLE);
    }

    private void stopLoading() {
        Log.d(TAG, "stopLoading()");
        posterProgress.setVisibility(View.GONE);
    }

    private void showError() {
        Log.d(TAG, "showError()");
        Snackbar.make(baseContainer, getString(R.string.error_common), Snackbar.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy()");
        super.onDestroy();
        viewModel.getStateLiveData().removeObservers(this);
    }
}
