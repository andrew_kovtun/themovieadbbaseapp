package com.moviedb.app.model.entity;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public final class Movie {
    @SerializedName("id")
    private final long id;
    @SerializedName("title")
    private final String title;
    @SerializedName("popularity")
    private final float popularity;
    @SerializedName("poster_path")
    private final String posterUri;

    public Movie(long id, String title, float popularity, String posterUri) {
        this.id = id;
        this.title = title;
        this.popularity = popularity;
        this.posterUri = posterUri;
    }

    public String getTitle() {
        return title;
    }

    public float getPopularity() {
        return popularity;
    }

    public String getPosterUri() {
        return posterUri;
    }

    public long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Movie)) return false;
        Movie movie = (Movie) o;
        return getId() == movie.getId() &&
                Float.compare(movie.getPopularity(), getPopularity()) == 0 &&
                Objects.equals(getTitle(), movie.getTitle()) &&
                Objects.equals(getPosterUri(), movie.getPosterUri());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTitle(), getPopularity(), getPosterUri());
    }
}
