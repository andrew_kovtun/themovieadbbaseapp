# Project:TheMoviedbBaseApp

Android native client for moviedb API

- Load 5 pages of popular movies (20 items per page)
- Search by movies
- Support device rotation
- Support full sized thumb picture
- App state preservation/restoration

## Technical solutions
* **Design** - MVVM with LiveData
* **Reactive** - RxJava2, RxAndroid
* **Network** - Retrofit2
* **ImageLoader** - Picasso


# Screens
![picture](img/screen_main.png =300x500)
![picture](img/screen_search.png =300x500)
![picture](img/screen_details.png =300x500)