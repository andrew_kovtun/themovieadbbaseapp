package com.moviedb.app.model.entity;

import com.google.gson.annotations.SerializedName;

public final class MoviesBase {
    @SerializedName("results")
    private MovieCollection movieCollection;

    public MovieCollection getMovieCollection() {
        return movieCollection;
    }
}
