package com.moviedb.app.viewmodel;

import android.os.Bundle;

import com.moviedb.app.model.entity.MovieDetails;
import com.moviedb.app.model.repository.Cache;
import com.moviedb.app.model.repository.SimpleCache;
import com.moviedb.app.model.usecase.MoviesUseCaseAPI;
import com.moviedb.app.model.usecase.NetworkService;
import com.moviedb.app.ui.entity.MovieDetailsUIResponse;
import com.moviedb.app.ui.state.State;
import com.moviedb.app.utils.RouterUtils;

import java.util.Collections;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class DetailsScreenViewModel extends ViewModel implements BaseScreenViewModel<MovieDetailsUIResponse> {
    private final MoviesUseCaseAPI movieUseCases = NetworkService.getInstance();
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private MutableLiveData<MovieDetailsUIResponse> stateLiveData = new MutableLiveData<>();
    private Cache<MovieDetails> cache = new SimpleCache<>();

    @Override
    public void onCreate(@Nullable Bundle bundle) {
        if (bundle != null && bundle.containsKey(RouterUtils.MOVIE_ID_EXTRA)) {
            loadMovieDetails(bundle.getLong(RouterUtils.MOVIE_ID_EXTRA));
        }
    }

    @Override
    public LiveData<MovieDetailsUIResponse> getStateLiveData() {
        return stateLiveData;
    }

    private void loadMovieDetails(long id) {
        if (cache.hasData()) {
            stateLiveData.setValue(new MovieDetailsUIResponse(State.COMPLETE, cache.getAll().get(0)));
            return;
        }
        compositeDisposable.add(movieUseCases.getMovieDetails(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(s -> {
                    cache.clear();
                    stateLiveData.setValue(new MovieDetailsUIResponse(State.START_LOADING, null));
                })
                .doOnNext(details -> cache.put(Collections.singletonList(details)))
                .doOnTerminate(() -> stateLiveData.setValue(new MovieDetailsUIResponse(State.STOP_LOADING, null)))
                .subscribe(
                        details -> stateLiveData.setValue(new MovieDetailsUIResponse(State.COMPLETE, details)),
                        e -> stateLiveData.setValue(new MovieDetailsUIResponse(State.ERROR, null))));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.dispose();
    }
}