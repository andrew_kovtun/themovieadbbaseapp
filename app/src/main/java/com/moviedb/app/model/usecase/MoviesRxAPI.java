package com.moviedb.app.model.usecase;

import com.moviedb.app.model.entity.MovieDetails;
import com.moviedb.app.model.entity.MoviesBase;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MoviesRxAPI {
    @GET("movie/popular")
    Observable<MoviesBase> listPopularMovies(@Query("api_key") String apiKey, @Query("language") String locale, @Query("page") int page);

    @GET("movie/{movie_id}")
    Observable<MovieDetails> getMovieDetails(@Path("movie_id") long movieID, @Query("api_key") String apiKey, @Query("language") String locale);

    @GET("search/movie")
    Observable<MoviesBase> searchMovies(@Query("api_key") String apiKey, @Query("language") String locale, @Query("query") String query, @Query("page") int page, @Query("include_adult") boolean adult);
}
