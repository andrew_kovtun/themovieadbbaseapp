package com.moviedb.app.viewmodel;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;

public interface BaseScreenViewModel<T> {

    void onCreate(@Nullable Bundle bundle);

    LiveData<T> getStateLiveData();
}
