package com.moviedb.app.ui.entity;

import com.moviedb.app.ui.state.State;

public abstract class UIResponse<T> {
    private final State state;
    private final T commonObject;

    UIResponse(State state, T commonObject) {
        this.state = state;
        this.commonObject = commonObject;
    }

    public State getState() {
        return state;
    }

    public T getCommonObject() {
        return commonObject;
    }
}
