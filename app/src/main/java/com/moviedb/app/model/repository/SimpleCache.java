package com.moviedb.app.model.repository;

import java.util.ArrayList;
import java.util.List;

public class SimpleCache<T> implements Cache<T> {

    private final List<T> cachedData = new ArrayList<>();

    @Override
    public void put(List<T> data) {
        cachedData.addAll(data);
    }

    @Override
    public List<T> getAll() {
        return new ArrayList<>(cachedData);
    }

    @Override
    public void clear() {
        cachedData.clear();
    }

    @Override
    public boolean hasData() {
        return !cachedData.isEmpty();
    }
}
