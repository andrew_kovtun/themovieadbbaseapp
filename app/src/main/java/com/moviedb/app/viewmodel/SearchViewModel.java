package com.moviedb.app.viewmodel;

import androidx.annotation.NonNull;

public interface SearchViewModel<T> extends BaseScreenViewModel<T> {

    void onSearch(@NonNull String query);
}
