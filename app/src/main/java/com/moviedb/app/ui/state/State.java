package com.moviedb.app.ui.state;

public enum State {
    COMPLETE,
    ERROR,
    START_LOADING,
    STOP_LOADING
}
