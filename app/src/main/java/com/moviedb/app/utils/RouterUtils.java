package com.moviedb.app.utils;

import android.content.Context;
import android.content.Intent;

import com.moviedb.app.ui.MovieDetailsActivity;

import androidx.annotation.NonNull;

public final class RouterUtils {

    public static final String MOVIE_ID_EXTRA = "MOVIE_ID_EXTRA";

    public static Intent buildMovieDetailsRouteAction(@NonNull Context ctx, long id) {
        Intent launchMovieDetailsIntent = new Intent(ctx, MovieDetailsActivity.class);
        launchMovieDetailsIntent.putExtra(MOVIE_ID_EXTRA, id);
        return launchMovieDetailsIntent;
    }
}
